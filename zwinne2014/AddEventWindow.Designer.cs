﻿namespace zwinne2014
{
    partial class addEventWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eventDate = new System.Windows.Forms.DateTimePicker();
            this.NameText = new System.Windows.Forms.TextBox();
            this.DescriptionText = new System.Windows.Forms.RichTextBox();
            this.addEventButton = new System.Windows.Forms.Button();
            this.Nazwa = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // eventDate
            // 
            this.eventDate.CustomFormat = "HH:mm, dd MMM yyyy";
            this.eventDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.eventDate.Location = new System.Drawing.Point(14, 15);
            this.eventDate.Name = "eventDate";
            this.eventDate.Size = new System.Drawing.Size(430, 26);
            this.eventDate.TabIndex = 0;
            // 
            // NameText
            // 
            this.NameText.Location = new System.Drawing.Point(81, 51);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(362, 26);
            this.NameText.TabIndex = 4;
            // 
            // DescriptionText
            // 
            this.DescriptionText.Location = new System.Drawing.Point(14, 128);
            this.DescriptionText.Name = "DescriptionText";
            this.DescriptionText.Size = new System.Drawing.Size(430, 206);
            this.DescriptionText.TabIndex = 6;
            this.DescriptionText.Text = "";
            // 
            // addEventButton
            // 
            this.addEventButton.Location = new System.Drawing.Point(267, 386);
            this.addEventButton.Name = "addEventButton";
            this.addEventButton.Size = new System.Drawing.Size(81, 35);
            this.addEventButton.TabIndex = 7;
            this.addEventButton.Text = "Dodaj";
            this.addEventButton.UseVisualStyleBackColor = true;
            this.addEventButton.Click += new System.EventHandler(this.addEventButton_Click);
            // 
            // Nazwa
            // 
            this.Nazwa.AutoSize = true;
            this.Nazwa.Location = new System.Drawing.Point(14, 55);
            this.Nazwa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Nazwa.Name = "Nazwa";
            this.Nazwa.Size = new System.Drawing.Size(57, 20);
            this.Nazwa.TabIndex = 8;
            this.Nazwa.Text = "Nazwa";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 105);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Opis";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(356, 386);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(82, 35);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // addEventWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 438);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Nazwa);
            this.Controls.Add(this.addEventButton);
            this.Controls.Add(this.DescriptionText);
            this.Controls.Add(this.NameText);
            this.Controls.Add(this.eventDate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Name = "addEventWindow";
            this.Text = "Dodawanie terminu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker eventDate;
        private System.Windows.Forms.TextBox NameText;
        private System.Windows.Forms.RichTextBox DescriptionText;
        private System.Windows.Forms.Button addEventButton;
        private System.Windows.Forms.Label Nazwa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
    }
}