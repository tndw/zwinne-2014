﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace zwinne2014
{
    public class Case
    {
        public int id;
        public string client;
        public string caseType;
        public Employee lawyer;
        public string description;
        public EventList eventlist;
        public int numberOfEvents; // nie jest tożsame z wielkością listy terminów, zbiera ilość wszystkich kiedykolwiek przypisanych terminów

        public Case(int id, string client, string caseType, Employee lawyer, string description, int numberOfEvents)
        {
            this.id = id;
            this.client = client;
            this.caseType = caseType;
            this.lawyer = lawyer;
            this.description = description;
            eventlist = new EventList();
            this.numberOfEvents = numberOfEvents;
        }
        public override string ToString()
        {
            return "Klient: "+client+", sprawa: "+caseType+", opiekun: "+lawyer;

        }
/*        public List<string> makeList ()
        {
            List<string> temp = new List<string> () 
            {
                this.client,
                this.caseType,
                this.lawyer.ToString()
            };
            return temp;
        }
*/
        public Event AddEvent( DateTime date, string name, string description )
        {
            numberOfEvents++;
            Event x = new Event(numberOfEvents, date, name, description, this);
            eventlist.add(x);
            return x;
        }
        public void SerializeToXml()
        {
            structCase serializeCase = new structCase();
            serializeCase.id = this.id;
            serializeCase.client = this.client;
            serializeCase.caseType = this.caseType;
            serializeCase.lawyerName = this.lawyer.name;
            serializeCase.description = this.description;
            serializeCase.numberOfEvents = this.numberOfEvents;

            StreamWriter writer = new StreamWriter("cases\\case" + this.id + ".xml");

            XmlSerializer serializer = new XmlSerializer(typeof(structCase));
            serializer.Serialize(writer, serializeCase);
            writer.Flush();
            writer.Close();
        }
    }
    [Serializable]
    public struct structCase
    {
        public int id;
        public string client;
        public string caseType;
        public string lawyerName;
        public string description;
        public int numberOfEvents;
    }
}
