﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zwinne2014
{
    public class CaseList
    {
        public List<Case> list;
        public CaseList()
        {
            list = new List<Case>();
        }
        public Case add(Case c)
        {
            list.Add(c);
            return c;
        }
    }
}
