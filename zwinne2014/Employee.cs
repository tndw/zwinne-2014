﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zwinne2014
{
    public class Employee
    {
        public string name { get; set; }
        public string password;
        public pos position;
        public Employee(string name, string password, pos position)
        {
            this.name = name;
            this.password = password;
            this.position = position;
        }
        public override string ToString()
        {
            return this.name;
        }
    }
    public enum pos { Secretary, Lawyer };
}
