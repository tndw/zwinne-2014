﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zwinne2014
{
    public class ErrorMessage : SystemException
    {
        public ErrorMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}
