﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace zwinne2014
{
    public class Event
    {
        public int id;
        public DateTime date;
        public string name;
        public string description;
        public Case assignedCase;
        public Event( int id, DateTime date, string name, string description, Case c)
        {
            this.id = id;
            this.date = date;
            this.name = name;
            this.description = description;
            assignedCase = c;
        }
        public override string ToString()
        {
            return date.TimeOfDay.ToString("hh") + ";" + date.TimeOfDay.ToString("mm") + ", " + assignedCase + ", termin: " + name;
        }
        public void SerializeToXml()
        {
            structEvent serializeEvent = new structEvent();
            serializeEvent.id = this.id;
            serializeEvent.date = this.date.ToString();
            serializeEvent.name = this.name;
            serializeEvent.description = this.description;
            serializeEvent.assignedCaseId = assignedCase.id;

            StreamWriter writer = new StreamWriter("events\\case" + assignedCase.id + "event" + this.id + ".xml");

            XmlSerializer serializer = new XmlSerializer(typeof(structEvent));
            serializer.Serialize(writer, serializeEvent);
            writer.Flush();
            writer.Close();
        }
    }
    [Serializable]
    public struct structEvent
    {
        public int id;
        public string date;
        public string name;
        public string description;
        public int assignedCaseId;
    }
}
