﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zwinne2014
{
    public class EventList
    {
        public List<Event> list;
        public EventList()
        {
            list = new List<Event>();
        }
        public void add(Event e)
        {
            list.Add(e);
        }
    }
}
