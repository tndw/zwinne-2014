﻿namespace zwinne2014
{
    partial class addCaseWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.clientName = new System.Windows.Forms.TextBox();
            this.caseType = new System.Windows.Forms.TextBox();
            this.caseDate = new System.Windows.Forms.DateTimePicker();
            this.assignedLawyer = new System.Windows.Forms.ComboBox();
            this.caseDescription = new System.Windows.Forms.RichTextBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Klient:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Typ Sprawy:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 155);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Przydzielony Prawnik:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 202);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Opis Sprawy:";
            // 
            // clientName
            // 
            this.clientName.Location = new System.Drawing.Point(207, 15);
            this.clientName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.clientName.Name = "clientName";
            this.clientName.Size = new System.Drawing.Size(298, 26);
            this.clientName.TabIndex = 5;
            // 
            // caseType
            // 
            this.caseType.Location = new System.Drawing.Point(207, 60);
            this.caseType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.caseType.Name = "caseType";
            this.caseType.Size = new System.Drawing.Size(298, 26);
            this.caseType.TabIndex = 6;
            // 
            // caseDate
            // 
            this.caseDate.Location = new System.Drawing.Point(207, 102);
            this.caseDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.caseDate.Name = "caseDate";
            this.caseDate.Size = new System.Drawing.Size(298, 26);
            this.caseDate.TabIndex = 7;
            // 
            // assignedLawyer
            // 
            this.assignedLawyer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.assignedLawyer.Location = new System.Drawing.Point(207, 151);
            this.assignedLawyer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.assignedLawyer.Name = "assignedLawyer";
            this.assignedLawyer.Size = new System.Drawing.Size(298, 28);
            this.assignedLawyer.Sorted = true;
            this.assignedLawyer.TabIndex = 8;
            // 
            // caseDescription
            // 
            this.caseDescription.Location = new System.Drawing.Point(24, 228);
            this.caseDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.caseDescription.Name = "caseDescription";
            this.caseDescription.Size = new System.Drawing.Size(481, 184);
            this.caseDescription.TabIndex = 9;
            this.caseDescription.Text = "";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(394, 423);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(112, 35);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(273, 423);
            this.addButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(112, 35);
            this.addButton.TabIndex = 11;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // addCaseWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 471);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.caseDescription);
            this.Controls.Add(this.assignedLawyer);
            this.Controls.Add(this.caseDate);
            this.Controls.Add(this.caseType);
            this.Controls.Add(this.clientName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "addCaseWindow";
            this.Text = "Dodaj Sprawę";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox clientName;
        private System.Windows.Forms.TextBox caseType;
        private System.Windows.Forms.DateTimePicker caseDate;
        private System.Windows.Forms.ComboBox assignedLawyer;
        private System.Windows.Forms.RichTextBox caseDescription;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addButton;
    }
}