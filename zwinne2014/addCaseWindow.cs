﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace zwinne2014
{
    public partial class addCaseWindow : Form
    {
        public Case newCase;
        public EmployeeList employeeList;
        public CaseList caseList;
        public mainWindow mainWin;
        public addCaseWindow(mainWindow m, EmployeeList elist, CaseList clist)
        {
            InitializeComponent();
            employeeList = elist;
            caseList = clist;
            mainWin = m;
            foreach (var i in employeeList.list)
            {
                if (i.position == pos.Lawyer)
                    assignedLawyer.Items.Add(i);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (System.DateTime.Today > caseDate.Value)
            {
                new ErrorMessage("Błąd: Podana data jest w przeszłości.");
            }
            else if (assignedLawyer.SelectedItem == null)
            {
                new ErrorMessage("Błąd: Nie wybrano prawnika.");
            }
            else
            {
                mainWin.loginWin.numberOfCases++;
                mainWin.setNumberOfCases( mainWin.loginWin.numberOfCases );

                newCase = new Case(mainWin.loginWin.numberOfCases,
                                   clientName.Text.ToString(), caseType.Text.ToString(),
                                   employeeList.list.Find(x => x.name == assignedLawyer.SelectedItem.ToString()),
                                   caseDescription.Text.ToString(),
                                   0);
                caseList.add(newCase);

                Event caseDateEvent = newCase.AddEvent(caseDate.Value, "Dodanie sprawy", "Dodanie sprawy do systemu.");
                caseDateEvent.SerializeToXml();

              //  mainWin.uploadFileOnServer("events", "case" + caseDateEvent.assignedCase.id + "event" + caseDateEvent.id);

               // File.Delete("events\\case" + caseDateEvent.assignedCase.id + "event" + caseDateEvent.id + ".xml");

                mainWin.refreshCases();
                mainWin.refreshEvents(caseDate.Value);
                MessageBox.Show("Sprawa dodana pomyślnie!");
                newCase.SerializeToXml();

               // mainWin.uploadFileOnServer("cases", "case" + newCase.id );

       //         File.Delete( "cases\\case" + newCase.id + ".xml" );

                this.Close();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}
