﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace zwinne2014
{
    public partial class addEventWindow : Form
    {
        public Case assignedCase;
        public mainWindow mainWin;
        public addEventWindow(Case assignedCase, mainWindow mainWin)
        {
            this.assignedCase = assignedCase;
            this.mainWin = mainWin;
            InitializeComponent();
        }
        private void addEventButton_Click(object sender, EventArgs e)
        {
            if (eventDate.Value >= DateTime.Today)
            {
                if (NameText.Text.ToString() != "")
                {
                    Event x = assignedCase.AddEvent(eventDate.Value, NameText.Text.ToString(), DescriptionText.Text.ToString());

                    x.SerializeToXml();
                    //mainWin.uploadFileOnServer("events", "case" + assignedCase.id + "event" + x.id);
                    //File.Delete("events\\case" + assignedCase.id + "event" + x.id + ".xml");

                    assignedCase.SerializeToXml();
                    //mainWin.uploadFileOnServer("cases", "case" + assignedCase.id);
                    //File.Delete("cases\\case" + assignedCase.id + ".xml");

                    mainWin.refreshEvents(eventDate.Value);
                    this.Close();
                }
                else
                {
                    new ErrorMessage("Błąd: Nie ma nazwy.");
                }
            }
            else
            {
                new ErrorMessage("Błąd: Zła data.");
            }
        }
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
