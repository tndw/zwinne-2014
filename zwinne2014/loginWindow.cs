﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace zwinne2014
{
    public partial class loginWindow : Form
    {
        public CaseList caseList;
        public EmployeeList employeeList;
        public int numberOfCases;
        public mainWindow next;
        public loginWindow()
        {
            InitializeComponent();
            employeeList = new EmployeeList();
            caseList = new CaseList();

            employeeList.list.Add(new Employee("login", "haslo", pos.Secretary));
            employeeList.list.Add(new Employee("andrzej", "qwerty", pos.Lawyer));

            next = new mainWindow(this, employeeList, caseList);

            //zrobić łapanie wyjątku gdy nie ma pliku
            //downloadFileFromServer("cases", "number_of_cases");
            if (File.Exists("cases\\number_of_cases.xml"))
            {
                numberOfCases = getNumberOfCases();
                //File.Delete("cases\\number_of_cases.xml");
            }
            else
                numberOfCases = 0;

            int caseCount = 1;
            while(  caseCount <= numberOfCases )
            {
                //downloadFileFromServer("cases", "case" + caseCount);
                if( File.Exists("cases\\case" + caseCount + ".xml" ) )
                {
                    structCase dCase = DeserializeCaseFromXml(caseCount);
                    Case d = caseList.add(new Case( caseCount,
                                                    dCase.client,
                                                    dCase.caseType,
                                                    employeeList.findByName(dCase.lawyerName),
                                                    dCase.description,
                                                    dCase.numberOfEvents) );
                    int eventCount = 1;
                    while(  eventCount <= dCase.numberOfEvents )
                    {
                  //      downloadFileFromServer( "events", "case" + d.id + "event" + eventCount );
                        if( File.Exists("events\\case" + d.id + "event" + eventCount + ".xml" ) )
                        {
                            structEvent dEvent = DeserializeEventFromXml(d.id, eventCount);
                            d.eventlist.add( new Event( eventCount,
                                                        Convert.ToDateTime(dEvent.date),
                                                        dEvent.name,
                                                        dEvent.description,
                                                        d ) );
                        }
                    //    File.Delete("events\\case" + d.id + "event" + eventCount + ".xml");
                        eventCount++;
                    }
                    //File.Delete("cases\\case" + caseCount + ".xml");
                }
                caseCount++;
            }
            next.refreshCases();
            next.refreshEvents(DateTime.Today);
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (employeeList.find(usernameTextBox.Text, passwordTextBox.Text))
            {
                display.Text = "Zalogowałeś się";
                next.Show();
                this.Hide();
            }
            else display.Text = "Podałeś złą nazwę użytkownika i hasło";
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public structCase DeserializeCaseFromXml(int number)
        {
            StreamReader reader = new StreamReader("cases\\case" + number + ".xml");
            XmlSerializer serializer = new XmlSerializer(typeof(structCase));
            structCase x = (structCase)serializer.Deserialize(reader);
            reader.Close();
            reader.Dispose();
            return x;
        }
        public structEvent DeserializeEventFromXml(int caseId, int number)
        {
            StreamReader reader = new StreamReader("events\\case" + caseId + "event" + number + ".xml");
            XmlSerializer serializer = new XmlSerializer(typeof(structEvent));
            structEvent x = (structEvent)serializer.Deserialize(reader);
            reader.Close();
            reader.Dispose();
            return x;
        }
        public int getNumberOfCases()
        {
            StreamReader reader = new StreamReader("cases\\number_of_cases.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(int));
            int x = (int)serializer.Deserialize(reader);
            reader.Close();
            reader.Dispose();
            return x;
        }
        //public void downloadFileFromServer(string folder, string filename)
        //{
        //    FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://piotrmiszkiel.com/public_html/zwinne_login/" + folder + "//" + filename + ".xml");
        //    request.Method = WebRequestMethods.Ftp.DownloadFile;

        //    request.Credentials = new NetworkCredential("zwinne_login@piotrmiszkiel.com", "qwerty123");

        //    FtpWebResponse response = (FtpWebResponse)request.GetResponse();

        //    Stream responseStream = response.GetResponseStream();

        //    using ( var fileStream = File.Create(folder + "\\" + filename + ".xml") )
        //    {
        //        responseStream.CopyTo(fileStream);
        //    }

        //    System.Diagnostics.Debug.WriteLine("Downloading of {0} complete, status {1}", filename, response.StatusDescription);
        //    response.Close();
        //    //deszyfrowanie
        //}
    }
}
