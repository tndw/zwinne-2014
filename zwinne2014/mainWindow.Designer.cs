﻿namespace zwinne2014
{
    partial class mainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Sprawy = new System.Windows.Forms.TabPage();
            this.deleteAllCasesButton = new System.Windows.Forms.Button();
            this.deleteCaseButton = new System.Windows.Forms.Button();
            this.AddEventButton = new System.Windows.Forms.Button();
            this.caseListBox = new System.Windows.Forms.ListBox();
            this.addCaseButton = new System.Windows.Forms.Button();
            this.Rozliczenia = new System.Windows.Forms.TabPage();
            this.Terminy = new System.Windows.Forms.TabPage();
            this.deleteEventButton = new System.Windows.Forms.Button();
            this.eventDateBox = new System.Windows.Forms.DateTimePicker();
            this.eventListBox = new System.Windows.Forms.ListBox();
            this.tabControl1.SuspendLayout();
            this.Sprawy.SuspendLayout();
            this.Terminy.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Sprawy);
            this.tabControl1.Controls.Add(this.Rozliczenia);
            this.tabControl1.Controls.Add(this.Terminy);
            this.tabControl1.Location = new System.Drawing.Point(18, 18);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1054, 543);
            this.tabControl1.TabIndex = 0;
            // 
            // Sprawy
            // 
            this.Sprawy.Controls.Add(this.deleteAllCasesButton);
            this.Sprawy.Controls.Add(this.deleteCaseButton);
            this.Sprawy.Controls.Add(this.AddEventButton);
            this.Sprawy.Controls.Add(this.caseListBox);
            this.Sprawy.Controls.Add(this.addCaseButton);
            this.Sprawy.Location = new System.Drawing.Point(4, 29);
            this.Sprawy.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Sprawy.Name = "Sprawy";
            this.Sprawy.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Sprawy.Size = new System.Drawing.Size(1046, 510);
            this.Sprawy.TabIndex = 0;
            this.Sprawy.Text = "Sprawy";
            this.Sprawy.UseVisualStyleBackColor = true;
            // 
            // deleteAllCasesButton
            // 
            this.deleteAllCasesButton.Location = new System.Drawing.Point(888, 11);
            this.deleteAllCasesButton.Name = "deleteAllCasesButton";
            this.deleteAllCasesButton.Size = new System.Drawing.Size(153, 34);
            this.deleteAllCasesButton.TabIndex = 5;
            this.deleteAllCasesButton.Text = "Usuń Wszystkie";
            this.deleteAllCasesButton.UseVisualStyleBackColor = true;
            this.deleteAllCasesButton.Visible = false;
            this.deleteAllCasesButton.Click += new System.EventHandler(this.deleteAllCasesButton_Click);
            // 
            // deleteCaseButton
            // 
            this.deleteCaseButton.Location = new System.Drawing.Point(290, 11);
            this.deleteCaseButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deleteCaseButton.Name = "deleteCaseButton";
            this.deleteCaseButton.Size = new System.Drawing.Size(124, 35);
            this.deleteCaseButton.TabIndex = 4;
            this.deleteCaseButton.Text = "Usuń Sprawę";
            this.deleteCaseButton.UseVisualStyleBackColor = true;
            this.deleteCaseButton.Click += new System.EventHandler(this.deleteCaseButton_Click);
            // 
            // AddEventButton
            // 
            this.AddEventButton.Location = new System.Drawing.Point(462, 11);
            this.AddEventButton.Name = "AddEventButton";
            this.AddEventButton.Size = new System.Drawing.Size(136, 35);
            this.AddEventButton.TabIndex = 2;
            this.AddEventButton.Text = "Dodaj termin";
            this.AddEventButton.UseVisualStyleBackColor = true;
            this.AddEventButton.Click += new System.EventHandler(this.AddEventButton_Click);
            // 
            // caseListBox
            // 
            this.caseListBox.FormattingEnabled = true;
            this.caseListBox.ItemHeight = 20;
            this.caseListBox.Location = new System.Drawing.Point(10, 68);
            this.caseListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.caseListBox.Name = "caseListBox";
            this.caseListBox.Size = new System.Drawing.Size(878, 424);
            this.caseListBox.TabIndex = 1;
            // 
            // addCaseButton
            // 
            this.addCaseButton.Location = new System.Drawing.Point(10, 11);
            this.addCaseButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addCaseButton.Name = "addCaseButton";
            this.addCaseButton.Size = new System.Drawing.Size(135, 35);
            this.addCaseButton.TabIndex = 0;
            this.addCaseButton.Text = "Dodaj Sprawę";
            this.addCaseButton.UseVisualStyleBackColor = true;
            this.addCaseButton.Click += new System.EventHandler(this.addCaseButton_Click);
            // 
            // Rozliczenia
            // 
            this.Rozliczenia.Location = new System.Drawing.Point(4, 29);
            this.Rozliczenia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Rozliczenia.Name = "Rozliczenia";
            this.Rozliczenia.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Rozliczenia.Size = new System.Drawing.Size(1046, 510);
            this.Rozliczenia.TabIndex = 1;
            this.Rozliczenia.Text = "Rozliczenia";
            this.Rozliczenia.UseVisualStyleBackColor = true;
            // 
            // Terminy
            // 
            this.Terminy.Controls.Add(this.deleteEventButton);
            this.Terminy.Controls.Add(this.eventDateBox);
            this.Terminy.Controls.Add(this.eventListBox);
            this.Terminy.Location = new System.Drawing.Point(4, 29);
            this.Terminy.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Terminy.Name = "Terminy";
            this.Terminy.Size = new System.Drawing.Size(1046, 510);
            this.Terminy.TabIndex = 2;
            this.Terminy.Text = "Terminy";
            this.Terminy.UseVisualStyleBackColor = true;
            // 
            // deleteEventButton
            // 
            this.deleteEventButton.Location = new System.Drawing.Point(12, 61);
            this.deleteEventButton.Name = "deleteEventButton";
            this.deleteEventButton.Size = new System.Drawing.Size(140, 40);
            this.deleteEventButton.TabIndex = 3;
            this.deleteEventButton.Text = "Usuń termin";
            this.deleteEventButton.UseVisualStyleBackColor = true;
            this.deleteEventButton.Click += new System.EventHandler(this.deleteEventButton_Click);
            // 
            // eventDateBox
            // 
            this.eventDateBox.CustomFormat = "HH:mm, dd MMM yyyy";
            this.eventDateBox.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.eventDateBox.Location = new System.Drawing.Point(12, 26);
            this.eventDateBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventDateBox.Name = "eventDateBox";
            this.eventDateBox.Size = new System.Drawing.Size(304, 26);
            this.eventDateBox.TabIndex = 2;
            this.eventDateBox.ValueChanged += new System.EventHandler(this.eventDateBox_ValueChanged);
            // 
            // eventListBox
            // 
            this.eventListBox.FormattingEnabled = true;
            this.eventListBox.ItemHeight = 20;
            this.eventListBox.Location = new System.Drawing.Point(327, 26);
            this.eventListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.eventListBox.Name = "eventListBox";
            this.eventListBox.Size = new System.Drawing.Size(690, 464);
            this.eventListBox.TabIndex = 1;
            // 
            // mainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 580);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "mainWindow";
            this.Text = "Menu Główne";
            this.tabControl1.ResumeLayout(false);
            this.Sprawy.ResumeLayout(false);
            this.Terminy.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Sprawy;
        private System.Windows.Forms.Button addCaseButton;
        private System.Windows.Forms.TabPage Rozliczenia;
        private System.Windows.Forms.ListBox caseListBox;
        private System.Windows.Forms.Button AddEventButton;
        private System.Windows.Forms.TabPage Terminy;
        private System.Windows.Forms.ListBox eventListBox;
        private System.Windows.Forms.DateTimePicker eventDateBox;
        private System.Windows.Forms.Button deleteCaseButton;
        private System.Windows.Forms.Button deleteAllCasesButton;
        private System.Windows.Forms.Button deleteEventButton;
    }
}