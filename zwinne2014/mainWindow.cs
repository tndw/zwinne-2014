﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Net;

namespace zwinne2014
{
    public partial class mainWindow : Form
    {
        public addCaseWindow newCase;
        public loginWindow loginWin;
        public EmployeeList employeeList;
        public CaseList caseList;
        public mainWindow(loginWindow l, EmployeeList eList, CaseList cList)
        {            
            loginWin = l;
            employeeList = eList;
            caseList = cList;
			//eventListBox.MultiColumn = true;
            InitializeComponent();
            this.refreshCases();
            #if DEBUG
            deleteAllCasesButton.Visible = true;
            #endif
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            loginWin.Close();
            base.OnFormClosing(e);
        }

        private void addCaseButton_Click(object sender, EventArgs e)
        {
            newCase = new addCaseWindow(this, employeeList, caseList);
            newCase.Show();
        }
/*        public void refreshCases()
        {
            caseListBox.Items.Clear();
            foreach (var i in caseList.list)
            {
                List<string> temp = i.makeList();
                if( !caseListBox.Items.Contains(i) ) caseListBox.Items.AddRange(new object[] 
                {
                    "{0}, ",temp[0],
                    temp[1]
                });
            }
        }
*/      
		public void refreshCases()
		{
			caseListBox.Items.Clear();
			foreach (var i in caseList.list)
			{
				if( !caseListBox.Items.Contains(i) ) caseListBox.Items.Add(i);
			}
		}
        public void deleteEvent(Event e)
        {
            e.assignedCase.numberOfEvents--;
            e.assignedCase.eventlist.list.Remove(e);
            refreshEvents(DateTime.Now);
        }
        public void deleteCase(Case c)
        {
            //FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://piotrmiszkiel.com/public_html/zwinne_login/cases/case" + c.id + ".xml");
            //request.Method = WebRequestMethods.Ftp.DeleteFile;

            //request.Credentials = new NetworkCredential("zwinne_login@piotrmiszkiel.com", "qwerty123");

            //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            //Console.WriteLine("Delete status: {0}", response.StatusDescription);
            //response.Close();
            loginWin.numberOfCases = 0;
            setNumberOfCases(0);

            c.eventlist.list.RemoveAll(x=>true);
            loginWin.caseList.list.Remove(c);
            refreshCases();
            refreshEvents(DateTime.Now);
        }
        public void deleteAllCases()
        {
            while (caseList.list.Count != 0)
                deleteCase(caseList.list[0]);
            refreshCases();
        }

        private void AddEventButton_Click(object sender, EventArgs e)
        {
            if (caseListBox.SelectedItem == null || !(caseListBox.SelectedItem is Case))
            {
                return;
            }
           addEventWindow newEvent = new addEventWindow((Case)caseListBox.SelectedItem,this);
           newEvent.Show();
        }

        private void eventDateBox_ValueChanged(object sender, EventArgs e)
        {
            refreshEvents(eventDateBox.Value);
        }
        
        public void refreshEvents( DateTime d )
        {
            eventListBox.Sorted = true;
            EventList eList = new EventList();

            foreach (var c in caseList.list)
            {
                foreach (var e in c.eventlist.list)
                {
                    if (e.date.Date == d.Date) eList.add(e);
                }
            }
            eventListBox.Items.Clear();
            
            foreach (var i in eList.list)
            {
                if (!eventListBox.Items.Contains(i)) eventListBox.Items.Add(i);
            }
        }
        //public void uploadFileOnServer(string folder, string filename)
        //{
        //    //szyfrowanie

        //    FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://piotrmiszkiel.com/public_html/zwinne_login/" + folder + "//" + filename + ".xml");
        //    request.Method = WebRequestMethods.Ftp.UploadFile;

        //    request.Credentials = new NetworkCredential("zwinne_login@piotrmiszkiel.com", "qwerty123");

        //    StreamReader sourceStream = new StreamReader(folder + "\\" + filename + ".xml");
        //    byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
        //    sourceStream.Close();
        //    request.ContentLength = fileContents.Length;

        //    Stream requestStream = request.GetRequestStream();
        //    requestStream.Write(fileContents, 0, fileContents.Length);
        //    requestStream.Close();

        //    FtpWebResponse response = (FtpWebResponse)request.GetResponse();

        //    Console.WriteLine("Uploading of {0} complete, status {1}", filename, response.StatusDescription);

        //    response.Close();
        //}

        private void deleteAllCasesButton_Click(object sender, EventArgs e)
        {
            deleteAllCases();
            refreshCases();
        }
        public void setNumberOfCases(int n)
        {
            StreamWriter writer = new StreamWriter("cases\\number_of_cases.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(int));
            serializer.Serialize(writer, n);
            writer.Flush();
            writer.Close();
            writer.Dispose();

            //uploadFileOnServer("cases", "number_of_cases");
            //File.Delete("cases\\number_of_cases.xml");
        }

        private void deleteCaseButton_Click(object sender, EventArgs e)
        {
            if (caseListBox.SelectedItem != null)
            {
                deleteCase((Case)caseListBox.SelectedItem);
                refreshCases();
            }
            else
            {
                new ErrorMessage("Błąd: Nie wybrano sprawy.");
            }
        }

        private void deleteEventButton_Click(object sender, EventArgs e)
        {
            if (eventListBox.SelectedItem != null)
            {
                deleteEvent((Event)eventListBox.SelectedItem);
                refreshCases();
            }
            else
            {
                new ErrorMessage("Błąd: Nie wybrano terminu.");
            }
        }

        private void deleteAllEvents_Click(object sender, EventArgs e)
        {

        }
    }
}
