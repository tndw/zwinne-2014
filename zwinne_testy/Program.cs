﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TestAutomationFX.UI.TestRunner;

namespace zwinne_testy
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //TestApplication.Run(new logowanie());
            //TestApplication.Run(new dodawanie_sprawy());
            TestApplication.RunMultiple(new logowanie(),
                                        new sprawy(),
                                        new terminy()/*,
                                        new dodawanie_sprawy_zla_data(),
                                        new dodawanie_terminu() */);
        }
    }
}
