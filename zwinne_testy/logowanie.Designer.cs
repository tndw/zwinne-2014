﻿namespace zwinne_testy
{
    partial class logowanie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(logowanie));
            this.Zwinne2014Application = new TestAutomationFX.UI.UIApplication();
            this.LoginWindow = new TestAutomationFX.UI.UIWindow();
            this.UsernameTextBox = new TestAutomationFX.UI.UIWindow();
            this.PasswordTextBox = new TestAutomationFX.UI.UIWindow();
            this.LoginButton = new TestAutomationFX.UI.UIWindow();
            this.Display = new TestAutomationFX.UI.UIWindow();
            this.Logowanie1 = new TestAutomationFX.UI.UIMsaa();
            this.CancelButton = new TestAutomationFX.UI.UIWindow();
            this.Label2 = new TestAutomationFX.UI.UIWindow();
            this.MainWindow = new TestAutomationFX.UI.UIWindow();
            this.TitleBar = new TestAutomationFX.UI.UIMsaa();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Zwinne2014Application
            // 
            this.Zwinne2014Application.Comment = null;
            this.Zwinne2014Application.ImagePath = "C:\\Users\\Piotrek\\Desktop\\tndw-zwinne-2014-9126b5529293\\zwinne2014\\bin\\Debug\\zwinn" +
    "e2014.exe";
            this.Zwinne2014Application.Name = "Zwinne2014Application";
            this.Zwinne2014Application.ObjectImage = null;
            this.Zwinne2014Application.Parent = null;
            this.Zwinne2014Application.ProcessName = "Zwinne2014";
            this.Zwinne2014Application.TimeOut = 1000;
            this.Zwinne2014Application.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Application;
            this.Zwinne2014Application.UseCoordinatesOnClick = false;
            this.Zwinne2014Application.UsedMatchedProperties = ((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties)((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.ProcessName | TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.CommandLineArguments)));
            this.Zwinne2014Application.WorkingDirectory = "C:\\Users\\Piotrek\\Desktop\\tndw-zwinne-2014-9126b5529293\\zwinne2014\\bin\\Debug";
            // 
            // LoginWindow
            // 
            this.LoginWindow.Comment = null;
            this.LoginWindow.InstanceName = "loginWindow";
            this.LoginWindow.MatchedIndex = 0;
            this.LoginWindow.MsaaName = "Logowanie";
            this.LoginWindow.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.LoginWindow.Name = "LoginWindow";
            this.LoginWindow.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("LoginWindow.ObjectImage")));
            this.LoginWindow.OwnedWindow = true;
            this.LoginWindow.Parent = this.Zwinne2014Application;
            this.LoginWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.LoginWindow.UseCoordinatesOnClick = true;
            this.LoginWindow.WindowClass = "";
            // 
            // UsernameTextBox
            // 
            this.UsernameTextBox.Comment = null;
            this.UsernameTextBox.Index = 4;
            this.UsernameTextBox.InstanceName = "usernameTextBox";
            this.UsernameTextBox.MatchedIndex = 0;
            this.UsernameTextBox.MsaaName = "Hasło:";
            this.UsernameTextBox.MsaaRole = System.Windows.Forms.AccessibleRole.Text;
            this.UsernameTextBox.Name = "UsernameTextBox";
            this.UsernameTextBox.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("UsernameTextBox.ObjectImage")));
            this.UsernameTextBox.Parent = this.LoginWindow;
            this.UsernameTextBox.UIObjectType = TestAutomationFX.UI.UIObjectTypes.TextBox;
            this.UsernameTextBox.UseCoordinatesOnClick = true;
            this.UsernameTextBox.WindowClass = "";
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Comment = null;
            this.PasswordTextBox.Index = 3;
            this.PasswordTextBox.InstanceName = "passwordTextBox";
            this.PasswordTextBox.MatchedIndex = 0;
            this.PasswordTextBox.MsaaName = null;
            this.PasswordTextBox.MsaaRole = System.Windows.Forms.AccessibleRole.Text;
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("PasswordTextBox.ObjectImage")));
            this.PasswordTextBox.Parent = this.LoginWindow;
            this.PasswordTextBox.UIObjectType = TestAutomationFX.UI.UIObjectTypes.TextBox;
            this.PasswordTextBox.UseCoordinatesOnClick = true;
            this.PasswordTextBox.WindowClass = "";
            // 
            // LoginButton
            // 
            this.LoginButton.Comment = null;
            this.LoginButton.Index = 1;
            this.LoginButton.InstanceName = "loginButton";
            this.LoginButton.MatchedIndex = 0;
            this.LoginButton.MsaaName = "Zaloguj";
            this.LoginButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("LoginButton.ObjectImage")));
            this.LoginButton.Parent = this.LoginWindow;
            this.LoginButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.LoginButton.UseCoordinatesOnClick = false;
            this.LoginButton.WindowClass = "";
            this.LoginButton.WindowText = "Zaloguj";
            // 
            // Display
            // 
            this.Display.Comment = null;
            this.Display.Index = 2;
            this.Display.InstanceName = "display";
            this.Display.MatchedIndex = 0;
            this.Display.MsaaName = "Podałeś złą nazwę użytkownika i hasło";
            this.Display.MsaaRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.Display.Name = "Display";
            this.Display.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Display.ObjectImage")));
            this.Display.Parent = this.LoginWindow;
            this.Display.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Label;
            this.Display.UseCoordinatesOnClick = true;
            this.Display.WindowClass = "";
            this.Display.WindowText = "Podałeś złą nazwę użytkownika i hasło";
            // 
            // Logowanie1
            // 
            this.Logowanie1.Comment = null;
            this.Logowanie1.MsaaName = "Logowanie";
            this.Logowanie1.Name = "Logowanie1";
            this.Logowanie1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Logowanie1.ObjectImage")));
            this.Logowanie1.Parent = this.LoginWindow;
            this.Logowanie1.Role = System.Windows.Forms.AccessibleRole.Client;
            this.Logowanie1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Logowanie1.UseCoordinatesOnClick = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Comment = null;
            this.CancelButton.Index = 0;
            this.CancelButton.InstanceName = "cancelButton";
            this.CancelButton.MatchedIndex = 0;
            this.CancelButton.MsaaName = "Anuluj";
            this.CancelButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("CancelButton.ObjectImage")));
            this.CancelButton.Parent = this.LoginWindow;
            this.CancelButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.CancelButton.UseCoordinatesOnClick = false;
            this.CancelButton.WindowClass = "";
            this.CancelButton.WindowText = "Anuluj";
            // 
            // Label2
            // 
            this.Label2.Comment = null;
            this.Label2.Index = 6;
            this.Label2.InstanceName = "label2";
            this.Label2.MatchedIndex = 0;
            this.Label2.MsaaName = "Nazwa użytkownika:";
            this.Label2.MsaaRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.Label2.Name = "Label2";
            this.Label2.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Label2.ObjectImage")));
            this.Label2.Parent = this.LoginWindow;
            this.Label2.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Label;
            this.Label2.UseCoordinatesOnClick = true;
            this.Label2.WindowClass = "";
            this.Label2.WindowText = "Nazwa użytkownika:";
            // 
            // MainWindow
            // 
            this.MainWindow.Comment = null;
            this.MainWindow.InstanceName = "mainWindow";
            this.MainWindow.MatchedIndex = 0;
            this.MainWindow.MsaaName = "Menu Główne";
            this.MainWindow.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.MainWindow.Name = "MainWindow";
            this.MainWindow.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("MainWindow.ObjectImage")));
            this.MainWindow.OwnedWindow = true;
            this.MainWindow.Parent = this.Zwinne2014Application;
            this.MainWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.MainWindow.UseCoordinatesOnClick = true;
            this.MainWindow.WindowClass = "";
            // 
            // TitleBar
            // 
            this.TitleBar.Comment = null;
            this.TitleBar.Index = 1;
            this.TitleBar.MsaaName = null;
            this.TitleBar.Name = "TitleBar";
            this.TitleBar.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TitleBar.ObjectImage")));
            this.TitleBar.Parent = this.MainWindow;
            this.TitleBar.Role = System.Windows.Forms.AccessibleRole.TitleBar;
            this.TitleBar.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Unknown;
            this.TitleBar.UseCoordinatesOnClick = true;
            // 
            // logowanie
            // 
            this.UIMapObjectApplications.Add(this.Zwinne2014Application);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private TestAutomationFX.UI.UIApplication Zwinne2014Application;
        private TestAutomationFX.UI.UIWindow LoginWindow;
        private TestAutomationFX.UI.UIWindow UsernameTextBox;
        private TestAutomationFX.UI.UIWindow PasswordTextBox;
        private TestAutomationFX.UI.UIWindow LoginButton;
        private TestAutomationFX.UI.UIWindow Display;
        private TestAutomationFX.UI.UIMsaa Logowanie1;
        private TestAutomationFX.UI.UIWindow CancelButton;
        private TestAutomationFX.UI.UIWindow Label2;
        private TestAutomationFX.UI.UIWindow MainWindow;
        private TestAutomationFX.UI.UIMsaa TitleBar;

    }
}
