﻿using System;
using System.ComponentModel;

using TestAutomationFX.Core;
using TestAutomationFX.UI;

namespace zwinne_testy
{
    [UITestFixture]
    public partial class logowanie : UIMap
    {
        public logowanie()
        {
            InitializeComponent();
        }

        public logowanie(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        [UITest()]
        public void zly_login_zle_haslo()
        {
            UsernameTextBox.Click(22, 13);
            Keyboard.SendKeys("a");
            PasswordTextBox.Click(26, 12);
            Keyboard.SendKeys("a");
            LoginButton.Click();
            Display.VerifyProperty("Text", "Podałeś złą nazwę użytkownika i hasło");
        }

        [UITest()]
        public void dobry_login_zle_haslo()
        {
            UsernameTextBox.Click(51, 10);
            UsernameTextBox.Drag(51, 10, Logowanie1, 201, 126);
            Keyboard.SendKeys("[BACKSPACE]");
            PasswordTextBox.Drag(56, 8, Logowanie1, 166, 162);
            Keyboard.SendKeys("[BACKSPACE]");
            UsernameTextBox.Click(16, 11);
            Keyboard.SendKeys("login");
            PasswordTextBox.Click(43, 21);
            Keyboard.SendKeys("a");
            LoginButton.Click();
            Display.VerifyProperty("Text", "Podałeś złą nazwę użytkownika i hasło");
        }

        [UITest()]
        public void zly_login_dobre_haslo()
        {
            UsernameTextBox.Drag(61, 14, Label2, 69, 5);
            Keyboard.SendKeys("a");
            PasswordTextBox.Drag(38, 13, Logowanie1, 231, 188);
            Keyboard.SendKeys("haslo");
            LoginButton.Click();
            Display.VerifyProperty("Text", "Podałeś złą nazwę użytkownika i hasło");
        }

        [UITest()]
        public void dobre_logowanie()
        {
            UsernameTextBox.Drag(103, 19, Label2, 130, 10);
            Keyboard.SendKeys("login");
            PasswordTextBox.Drag(79, 10, Logowanie1, 280, 185);
            Keyboard.SendKeys("haslo");
            LoginButton.Click();
            TitleBar.VerifyProperty("Text", "Menu Główne");
        }
    }
}
