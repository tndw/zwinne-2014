﻿namespace zwinne_testy
{
    partial class sprawy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sprawy));
            this.Zwinne2014Application = new TestAutomationFX.UI.UIApplication();
            this.MainWindow = new TestAutomationFX.UI.UIWindow();
            this.TabControl1 = new TestAutomationFX.UI.UIWindow();
            this.Sprawy1 = new TestAutomationFX.UI.UIWindow();
            this.AddCaseButton = new TestAutomationFX.UI.UIWindow();
            this.AddCaseWindow = new TestAutomationFX.UI.UIWindow();
            this.TitleBar = new TestAutomationFX.UI.UIMsaa();
            this.CaseDate = new TestAutomationFX.UI.UIWindow();
            this.AddButton = new TestAutomationFX.UI.UIWindow();
            this.Window = new TestAutomationFX.UI.UIWindow();
            this.BłądPodanadatajestwprzeszłości = new TestAutomationFX.UI.UIWindow();
            this.ButtonOK = new TestAutomationFX.UI.UIWindow();
            this.ExplorerApplication = new TestAutomationFX.UI.UIApplication();
            this.DeleteAllCasesButton = new TestAutomationFX.UI.UIWindow();
            this.BłądNiewybranoprawnika = new TestAutomationFX.UI.UIWindow();
            this.Sprawadodanapomyślnie = new TestAutomationFX.UI.UIWindow();
            this.Window1 = new TestAutomationFX.UI.UIWindow();
            this.CalendarControl = new TestAutomationFX.UI.UIWindow();
            this.TodayButton = new TestAutomationFX.UI.UIMsaa();
            this.AssignedLawyer = new TestAutomationFX.UI.UIWindow();
            this.Window2 = new TestAutomationFX.UI.UIWindow();
            this.uiWindow1 = new TestAutomationFX.UI.UIWindow();
            this.uiWindow2 = new TestAutomationFX.UI.UIWindow();
            this.Runningapplications = new TestAutomationFX.UI.UIWindow();
            this.Runningapplications1 = new TestAutomationFX.UI.UIWindow();
            this.Zwinne2014 = new TestAutomationFX.UI.UIMsaa();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Zwinne2014Application
            // 
            this.Zwinne2014Application.Comment = null;
            this.Zwinne2014Application.ImagePath = "D:\\tndw-zwinne-2014-9126b5529293\\zwinne2014\\bin\\Debug\\zwinne2014.exe";
            this.Zwinne2014Application.Name = "Zwinne2014Application";
            this.Zwinne2014Application.ObjectImage = null;
            this.Zwinne2014Application.Parent = null;
            this.Zwinne2014Application.ProcessName = "Zwinne2014";
            this.Zwinne2014Application.TimeOut = 1000;
            this.Zwinne2014Application.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Application;
            this.Zwinne2014Application.UseCoordinatesOnClick = false;
            this.Zwinne2014Application.UsedMatchedProperties = ((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties)((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.ProcessName | TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.CommandLineArguments)));
            this.Zwinne2014Application.WorkingDirectory = "D:\\tndw-zwinne-2014-9126b5529293\\zwinne2014\\bin\\Debug";
            // 
            // MainWindow
            // 
            this.MainWindow.Comment = null;
            this.MainWindow.InstanceName = "mainWindow";
            this.MainWindow.MatchedIndex = 0;
            this.MainWindow.MsaaName = "Menu Główne";
            this.MainWindow.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.MainWindow.Name = "MainWindow";
            this.MainWindow.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("MainWindow.ObjectImage")));
            this.MainWindow.OwnedWindow = true;
            this.MainWindow.Parent = this.Zwinne2014Application;
            this.MainWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.MainWindow.UseCoordinatesOnClick = true;
            this.MainWindow.WindowClass = "";
            // 
            // TabControl1
            // 
            this.TabControl1.Comment = null;
            this.TabControl1.Index = 0;
            this.TabControl1.InstanceName = "tabControl1";
            this.TabControl1.MatchedIndex = 0;
            this.TabControl1.MsaaName = null;
            this.TabControl1.MsaaRole = System.Windows.Forms.AccessibleRole.PageTabList;
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TabControl1.ObjectImage")));
            this.TabControl1.Parent = this.MainWindow;
            this.TabControl1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.TabControl;
            this.TabControl1.UseCoordinatesOnClick = true;
            this.TabControl1.WindowClass = "";
            // 
            // Sprawy1
            // 
            this.Sprawy1.Comment = null;
            this.Sprawy1.Index = 0;
            this.Sprawy1.InstanceName = "Sprawy";
            this.Sprawy1.MatchedIndex = 0;
            this.Sprawy1.MsaaName = "Sprawy";
            this.Sprawy1.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.Sprawy1.Name = "Sprawy1";
            this.Sprawy1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Sprawy1.ObjectImage")));
            this.Sprawy1.Parent = this.TabControl1;
            this.Sprawy1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Sprawy1.UseCoordinatesOnClick = true;
            this.Sprawy1.WindowClass = "";
            this.Sprawy1.WindowText = "Sprawy";
            // 
            // AddCaseButton
            // 
            this.AddCaseButton.Comment = null;
            this.AddCaseButton.Index = 4;
            this.AddCaseButton.InstanceName = "addCaseButton";
            this.AddCaseButton.MatchedIndex = 0;
            this.AddCaseButton.MsaaName = "Dodaj Sprawę";
            this.AddCaseButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.AddCaseButton.Name = "AddCaseButton";
            this.AddCaseButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddCaseButton.ObjectImage")));
            this.AddCaseButton.Parent = this.Sprawy1;
            this.AddCaseButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.AddCaseButton.UseCoordinatesOnClick = false;
            this.AddCaseButton.WindowClass = "";
            this.AddCaseButton.WindowText = "Dodaj Sprawę";
            // 
            // AddCaseWindow
            // 
            this.AddCaseWindow.Comment = null;
            this.AddCaseWindow.InstanceName = "addCaseWindow";
            this.AddCaseWindow.MatchedIndex = 0;
            this.AddCaseWindow.MsaaName = "Dodaj Sprawę";
            this.AddCaseWindow.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.AddCaseWindow.Name = "AddCaseWindow";
            this.AddCaseWindow.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddCaseWindow.ObjectImage")));
            this.AddCaseWindow.OwnedWindow = true;
            this.AddCaseWindow.Parent = this.Zwinne2014Application;
            this.AddCaseWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.AddCaseWindow.UseCoordinatesOnClick = true;
            this.AddCaseWindow.WindowClass = "";
            // 
            // TitleBar
            // 
            this.TitleBar.Comment = null;
            this.TitleBar.Index = 1;
            this.TitleBar.MsaaName = null;
            this.TitleBar.Name = "TitleBar";
            this.TitleBar.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TitleBar.ObjectImage")));
            this.TitleBar.Parent = this.AddCaseWindow;
            this.TitleBar.Role = System.Windows.Forms.AccessibleRole.TitleBar;
            this.TitleBar.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Unknown;
            this.TitleBar.UseCoordinatesOnClick = true;
            // 
            // CaseDate
            // 
            this.CaseDate.Comment = null;
            this.CaseDate.Index = 4;
            this.CaseDate.InstanceName = "caseDate";
            this.CaseDate.MatchedIndex = 0;
            this.CaseDate.MsaaName = "6 lutego 2015";
            this.CaseDate.MsaaRole = System.Windows.Forms.AccessibleRole.DropList;
            this.CaseDate.Name = "CaseDate";
            this.CaseDate.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("CaseDate.ObjectImage")));
            this.CaseDate.Parent = this.AddCaseWindow;
            this.CaseDate.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.CaseDate.UseCoordinatesOnClick = true;
            this.CaseDate.WindowClass = "";
            this.CaseDate.WindowText = "6 lutego 2015";
            // 
            // AddButton
            // 
            this.AddButton.Comment = null;
            this.AddButton.Index = 0;
            this.AddButton.InstanceName = "addButton";
            this.AddButton.MatchedIndex = 0;
            this.AddButton.MsaaName = "Dodaj";
            this.AddButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.AddButton.Name = "AddButton";
            this.AddButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddButton.ObjectImage")));
            this.AddButton.Parent = this.AddCaseWindow;
            this.AddButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.AddButton.UseCoordinatesOnClick = false;
            this.AddButton.WindowClass = "";
            this.AddButton.WindowText = "Dodaj";
            // 
            // Window
            // 
            this.Window.Comment = null;
            this.Window.Index = 0;
            this.Window.MatchedIndex = 0;
            this.Window.MsaaName = null;
            this.Window.MsaaRole = System.Windows.Forms.AccessibleRole.Dialog;
            this.Window.Name = "Window";
            this.Window.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Window.ObjectImage")));
            this.Window.OwnedWindow = true;
            this.Window.Parent = this.AddCaseWindow;
            this.Window.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Window.UseCoordinatesOnClick = true;
            this.Window.WindowClass = "#32770";
            // 
            // BłądPodanadatajestwprzeszłości
            // 
            this.BłądPodanadatajestwprzeszłości.Comment = null;
            this.BłądPodanadatajestwprzeszłości.Index = 1;
            this.BłądPodanadatajestwprzeszłości.MatchedIndex = 0;
            this.BłądPodanadatajestwprzeszłości.MsaaName = "Błąd: Podana data jest w przeszłości.";
            this.BłądPodanadatajestwprzeszłości.MsaaRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.BłądPodanadatajestwprzeszłości.Name = "BłądPodanadatajestwprzeszłości";
            this.BłądPodanadatajestwprzeszłości.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("BłądPodanadatajestwprzeszłości.ObjectImage")));
            this.BłądPodanadatajestwprzeszłości.Parent = this.Window;
            this.BłądPodanadatajestwprzeszłości.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.BłądPodanadatajestwprzeszłości.UseCoordinatesOnClick = true;
            this.BłądPodanadatajestwprzeszłości.WindowClass = "Static";
            this.BłądPodanadatajestwprzeszłości.WindowText = "Błąd: Podana data jest w przeszłości.";
            // 
            // ButtonOK
            // 
            this.ButtonOK.Comment = null;
            this.ButtonOK.Index = 0;
            this.ButtonOK.MatchedIndex = 0;
            this.ButtonOK.MsaaName = "OK";
            this.ButtonOK.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("ButtonOK.ObjectImage")));
            this.ButtonOK.Parent = this.Window;
            this.ButtonOK.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.ButtonOK.UseCoordinatesOnClick = false;
            this.ButtonOK.WindowClass = "Button";
            this.ButtonOK.WindowText = "OK";
            // 
            // ExplorerApplication
            // 
            this.ExplorerApplication.Comment = null;
            this.ExplorerApplication.ImagePath = "C:\\WINDOWS\\Explorer.EXE";
            this.ExplorerApplication.Name = "ExplorerApplication";
            this.ExplorerApplication.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("ExplorerApplication.ObjectImage")));
            this.ExplorerApplication.Parent = null;
            this.ExplorerApplication.ProcessName = "Explorer";
            this.ExplorerApplication.TimeOut = 1000;
            this.ExplorerApplication.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Application;
            this.ExplorerApplication.UseCoordinatesOnClick = false;
            this.ExplorerApplication.UsedMatchedProperties = ((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties)((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.ProcessName | TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.CommandLineArguments)));
            this.ExplorerApplication.WorkingDirectory = "C:\\WINDOWS";
            // 
            // DeleteAllCasesButton
            // 
            this.DeleteAllCasesButton.Comment = null;
            this.DeleteAllCasesButton.Index = 0;
            this.DeleteAllCasesButton.InstanceName = "deleteAllCasesButton";
            this.DeleteAllCasesButton.MatchedIndex = 0;
            this.DeleteAllCasesButton.MsaaName = "Usuń Wszystkie";
            this.DeleteAllCasesButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.DeleteAllCasesButton.Name = "DeleteAllCasesButton";
            this.DeleteAllCasesButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("DeleteAllCasesButton.ObjectImage")));
            this.DeleteAllCasesButton.Parent = this.Sprawy1;
            this.DeleteAllCasesButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.DeleteAllCasesButton.UseCoordinatesOnClick = false;
            this.DeleteAllCasesButton.WindowClass = "";
            this.DeleteAllCasesButton.WindowText = "Usuń Wszystkie";
            // 
            // BłądNiewybranoprawnika
            // 
            this.BłądNiewybranoprawnika.Comment = null;
            this.BłądNiewybranoprawnika.Index = 1;
            this.BłądNiewybranoprawnika.MatchedIndex = 0;
            this.BłądNiewybranoprawnika.MsaaName = "Błąd: Nie wybrano prawnika.";
            this.BłądNiewybranoprawnika.MsaaRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.BłądNiewybranoprawnika.Name = "BłądNiewybranoprawnika";
            this.BłądNiewybranoprawnika.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("BłądNiewybranoprawnika.ObjectImage")));
            this.BłądNiewybranoprawnika.Parent = this.Window;
            this.BłądNiewybranoprawnika.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.BłądNiewybranoprawnika.UseCoordinatesOnClick = true;
            this.BłądNiewybranoprawnika.WindowClass = "Static";
            this.BłądNiewybranoprawnika.WindowText = "Błąd: Nie wybrano prawnika.";
            // 
            // Sprawadodanapomyślnie
            // 
            this.Sprawadodanapomyślnie.Comment = null;
            this.Sprawadodanapomyślnie.Index = 1;
            this.Sprawadodanapomyślnie.MatchedIndex = 0;
            this.Sprawadodanapomyślnie.MsaaName = "Sprawa dodana pomyślnie!";
            this.Sprawadodanapomyślnie.MsaaRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.Sprawadodanapomyślnie.Name = "Sprawadodanapomyślnie";
            this.Sprawadodanapomyślnie.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Sprawadodanapomyślnie.ObjectImage")));
            this.Sprawadodanapomyślnie.Parent = this.Window;
            this.Sprawadodanapomyślnie.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Sprawadodanapomyślnie.UseCoordinatesOnClick = true;
            this.Sprawadodanapomyślnie.WindowClass = "Static";
            this.Sprawadodanapomyślnie.WindowText = "Sprawa dodana pomyślnie!";
            // 
            // Window1
            // 
            this.Window1.Comment = null;
            this.Window1.Index = 0;
            this.Window1.MatchedIndex = 0;
            this.Window1.MsaaName = null;
            this.Window1.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.Window1.Name = "Window1";
            this.Window1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Window1.ObjectImage")));
            this.Window1.OwnedWindow = true;
            this.Window1.Parent = this.AddCaseWindow;
            this.Window1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Window1.UseCoordinatesOnClick = true;
            this.Window1.WindowClass = "DropDown";
            // 
            // CalendarControl
            // 
            this.CalendarControl.Comment = null;
            this.CalendarControl.Index = 0;
            this.CalendarControl.MatchedIndex = 0;
            this.CalendarControl.MsaaName = "Calendar Control";
            this.CalendarControl.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.CalendarControl.Name = "CalendarControl";
            this.CalendarControl.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("CalendarControl.ObjectImage")));
            this.CalendarControl.Parent = this.Window1;
            this.CalendarControl.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.CalendarControl.UseCoordinatesOnClick = true;
            this.CalendarControl.WindowClass = "SysMonthCal32";
            // 
            // TodayButton
            // 
            this.TodayButton.Comment = null;
            this.TodayButton.MsaaName = "Today Button";
            this.TodayButton.Name = "TodayButton";
            this.TodayButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TodayButton.ObjectImage")));
            this.TodayButton.Parent = this.CalendarControl;
            this.TodayButton.Role = System.Windows.Forms.AccessibleRole.PushButton;
            this.TodayButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.TodayButton.UseCoordinatesOnClick = false;
            // 
            // AssignedLawyer
            // 
            this.AssignedLawyer.Comment = null;
            this.AssignedLawyer.Index = 3;
            this.AssignedLawyer.InstanceName = "assignedLawyer";
            this.AssignedLawyer.MatchedIndex = 0;
            this.AssignedLawyer.MsaaName = null;
            this.AssignedLawyer.MsaaRole = System.Windows.Forms.AccessibleRole.ComboBox;
            this.AssignedLawyer.Name = "AssignedLawyer";
            this.AssignedLawyer.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AssignedLawyer.ObjectImage")));
            this.AssignedLawyer.Parent = this.AddCaseWindow;
            this.AssignedLawyer.UIObjectType = TestAutomationFX.UI.UIObjectTypes.ComboBox;
            this.AssignedLawyer.UseCoordinatesOnClick = true;
            this.AssignedLawyer.WindowClass = "";
            // 
            // Window2
            // 
            this.Window2.Comment = null;
            this.Window2.Index = 18;
            this.Window2.MatchedIndex = 0;
            this.Window2.MsaaName = null;
            this.Window2.MsaaRole = System.Windows.Forms.AccessibleRole.List;
            this.Window2.Name = "Window2";
            this.Window2.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Window2.ObjectImage")));
            this.Window2.OwnedWindow = true;
            this.Window2.Parent = this.Zwinne2014Application;
            this.Window2.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Window2.UseCoordinatesOnClick = true;
            this.Window2.WindowClass = "ComboLBox";
            // 
            // uiWindow1
            // 
            this.uiWindow1.Comment = null;
            this.uiWindow1.MatchedIndex = 0;
            this.uiWindow1.MsaaName = null;
            this.uiWindow1.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.uiWindow1.Name = "uiWindow1";
            this.uiWindow1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("uiWindow1.ObjectImage")));
            this.uiWindow1.OwnedWindow = true;
            this.uiWindow1.Parent = this.ExplorerApplication;
            this.uiWindow1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.uiWindow1.UseCoordinatesOnClick = true;
            this.uiWindow1.WindowClass = "Shell_TrayWnd";
            // 
            // uiWindow2
            // 
            this.uiWindow2.Comment = null;
            this.uiWindow2.Index = 5;
            this.uiWindow2.MatchedIndex = 0;
            this.uiWindow2.MsaaName = null;
            this.uiWindow2.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.uiWindow2.Name = "uiWindow2";
            this.uiWindow2.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("uiWindow2.ObjectImage")));
            this.uiWindow2.Parent = this.uiWindow1;
            this.uiWindow2.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.uiWindow2.UseCoordinatesOnClick = true;
            this.uiWindow2.WindowClass = "ReBarWindow32";
            // 
            // Runningapplications
            // 
            this.Runningapplications.Comment = null;
            this.Runningapplications.Index = 0;
            this.Runningapplications.MatchedIndex = 0;
            this.Runningapplications.MsaaName = "Running applications";
            this.Runningapplications.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.Runningapplications.Name = "Runningapplications";
            this.Runningapplications.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Runningapplications.ObjectImage")));
            this.Runningapplications.Parent = this.uiWindow2;
            this.Runningapplications.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Runningapplications.UseCoordinatesOnClick = true;
            this.Runningapplications.WindowClass = "MSTaskSwWClass";
            this.Runningapplications.WindowText = "Running applications";
            // 
            // Runningapplications1
            // 
            this.Runningapplications1.Comment = null;
            this.Runningapplications1.Index = 0;
            this.Runningapplications1.MatchedIndex = 0;
            this.Runningapplications1.MsaaName = "Running applications";
            this.Runningapplications1.MsaaRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.Runningapplications1.Name = "Runningapplications1";
            this.Runningapplications1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Runningapplications1.ObjectImage")));
            this.Runningapplications1.Parent = this.Runningapplications;
            this.Runningapplications1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Runningapplications1.UseCoordinatesOnClick = true;
            this.Runningapplications1.WindowClass = "MSTaskListWClass";
            this.Runningapplications1.WindowText = "Running applications";
            // 
            // Zwinne2014
            // 
            this.Zwinne2014.Comment = null;
            this.Zwinne2014.MsaaName = "zwinne2014";
            this.Zwinne2014.Name = "Zwinne2014";
            this.Zwinne2014.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Zwinne2014.ObjectImage")));
            this.Zwinne2014.Parent = this.Runningapplications1;
            this.Zwinne2014.Role = System.Windows.Forms.AccessibleRole.ButtonMenu;
            this.Zwinne2014.UIObjectType = TestAutomationFX.UI.UIObjectTypes.MenuItem;
            this.Zwinne2014.UseCoordinatesOnClick = false;
            // 
            // sprawy
            // 
            this.UIMapObjectApplications.Add(this.Zwinne2014Application);
            this.UIMapObjectApplications.Add(this.ExplorerApplication);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private TestAutomationFX.UI.UIApplication Zwinne2014Application;
        private TestAutomationFX.UI.UIWindow MainWindow;
        private TestAutomationFX.UI.UIWindow TabControl1;
        private TestAutomationFX.UI.UIWindow Sprawy1;
        private TestAutomationFX.UI.UIWindow AddCaseButton;
        private TestAutomationFX.UI.UIWindow AddCaseWindow;
        private TestAutomationFX.UI.UIMsaa TitleBar;
        private TestAutomationFX.UI.UIWindow CaseDate;
        private TestAutomationFX.UI.UIWindow AddButton;
        private TestAutomationFX.UI.UIWindow Window;
        private TestAutomationFX.UI.UIWindow BłądPodanadatajestwprzeszłości;
        private TestAutomationFX.UI.UIWindow ButtonOK;
        private TestAutomationFX.UI.UIApplication ExplorerApplication;
        private TestAutomationFX.UI.UIWindow DeleteAllCasesButton;
        private TestAutomationFX.UI.UIWindow BłądNiewybranoprawnika;
        private TestAutomationFX.UI.UIWindow Sprawadodanapomyślnie;
        private TestAutomationFX.UI.UIWindow Window1;
        private TestAutomationFX.UI.UIWindow CalendarControl;
        private TestAutomationFX.UI.UIMsaa TodayButton;
        private TestAutomationFX.UI.UIWindow AssignedLawyer;
        private TestAutomationFX.UI.UIWindow Window2;
        private TestAutomationFX.UI.UIWindow uiWindow1;
        private TestAutomationFX.UI.UIWindow uiWindow2;
        private TestAutomationFX.UI.UIWindow Runningapplications;
        private TestAutomationFX.UI.UIWindow Runningapplications1;
        private TestAutomationFX.UI.UIMsaa Zwinne2014;


    }
}
