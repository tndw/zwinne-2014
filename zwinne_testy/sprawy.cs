﻿using System;
using System.ComponentModel;

using TestAutomationFX.Core;
using TestAutomationFX.UI;

namespace zwinne_testy
{
    [UITestFixture]
    public partial class sprawy : UIMap
    {
        public sprawy()
        {
            InitializeComponent();
        }

        public sprawy(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        [UITest()]
        public void zla_data()
        {
            AddCaseButton.Click();
            CaseDate.Click(13, 11);
            Keyboard.SendKeys("1");
            CaseDate.Click(66, 16);
            Keyboard.SendKeys("1");
            CaseDate.Click(139, 11);
            Keyboard.SendKeys("2000");
            AddButton.Click();
            BłądPodanadatajestwprzeszłości.VerifyProperty("Text", "Błąd: Podana data jest w przeszłości.");
            ButtonOK.Click();
        }

        [UITest()]
        public void dobre_dane()
        {
            CaseDate.Click(289, 12);
            TodayButton.Click();
            AddButton.Click();
            BłądNiewybranoprawnika.VerifyProperty("Text", "Błąd: Nie wybrano prawnika.");
            ButtonOK.Click();
            AssignedLawyer.Click(40, 16);
            Window2.Click(39, 11);
            AddButton.Click();
            Sprawadodanapomyślnie.VerifyProperty("Text", "Sprawa dodana pomyślnie!");
            ButtonOK.Click();
            DeleteAllCasesButton.Click();
        }
    }
}
