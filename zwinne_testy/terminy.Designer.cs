﻿namespace zwinne_testy
{
    partial class terminy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(terminy));
            this.Zwinne2014Application = new TestAutomationFX.UI.UIApplication();
            this.MainWindow = new TestAutomationFX.UI.UIWindow();
            this.TabControl1 = new TestAutomationFX.UI.UIWindow();
            this.Sprawy = new TestAutomationFX.UI.UIWindow();
            this.AddCaseButton = new TestAutomationFX.UI.UIWindow();
            this.CaseListBox = new TestAutomationFX.UI.UIWindow();
            this.uiMsaa1 = new TestAutomationFX.UI.UIMsaa();
            this.AddEventButton = new TestAutomationFX.UI.UIWindow();
            this.AddCaseWindow = new TestAutomationFX.UI.UIWindow();
            this.AssignedLawyer = new TestAutomationFX.UI.UIWindow();
            this.AddButton = new TestAutomationFX.UI.UIWindow();
            this.Window = new TestAutomationFX.UI.UIWindow();
            this.ButtonOK = new TestAutomationFX.UI.UIWindow();
            this.Window1 = new TestAutomationFX.UI.UIWindow();
            this.AddEventWindow = new TestAutomationFX.UI.UIWindow();
            this.TitleBar = new TestAutomationFX.UI.UIMsaa();
            this.EventDate = new TestAutomationFX.UI.UIWindow();
            this.AddEventButton1 = new TestAutomationFX.UI.UIWindow();
            this.Window2 = new TestAutomationFX.UI.UIWindow();
            this.BłądZładata = new TestAutomationFX.UI.UIWindow();
            this.ButtonOK1 = new TestAutomationFX.UI.UIWindow();
            this.BłądNiemanazwy = new TestAutomationFX.UI.UIWindow();
            this.Window3 = new TestAutomationFX.UI.UIWindow();
            this.CalendarControl = new TestAutomationFX.UI.UIWindow();
            this.TodayButton = new TestAutomationFX.UI.UIMsaa();
            this.NameText = new TestAutomationFX.UI.UIWindow();
            this.DeleteAllCasesButton = new TestAutomationFX.UI.UIWindow();
            this.TitleBar1 = new TestAutomationFX.UI.UIMsaa();
            this.Zwinne2014MainWindow = new TestAutomationFX.UI.UIMainWindow();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Zwinne2014Application
            // 
            this.Zwinne2014Application.Comment = null;
            this.Zwinne2014Application.ImagePath = "C:\\Users\\Piotrek\\Desktop\\tndw-zwinne-2014-9126b5529293\\zwinne2014\\bin\\Debug\\zwinn" +
    "e2014.exe";
            this.Zwinne2014Application.Name = "Zwinne2014Application";
            this.Zwinne2014Application.ObjectImage = null;
            this.Zwinne2014Application.Parent = null;
            this.Zwinne2014Application.ProcessName = "Zwinne2014";
            this.Zwinne2014Application.TimeOut = 1000;
            this.Zwinne2014Application.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Application;
            this.Zwinne2014Application.UseCoordinatesOnClick = false;
            this.Zwinne2014Application.UsedMatchedProperties = ((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties)((TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.ProcessName | TestAutomationFX.UI.UIApplication.UIApplicationMatchedProperties.CommandLineArguments)));
            this.Zwinne2014Application.WorkingDirectory = "C:\\Users\\Piotrek\\Desktop\\tndw-zwinne-2014-9126b5529293\\zwinne2014\\bin\\Debug";
            // 
            // MainWindow
            // 
            this.MainWindow.Comment = null;
            this.MainWindow.InstanceName = "mainWindow";
            this.MainWindow.MatchedIndex = 0;
            this.MainWindow.MsaaName = "Menu Główne";
            this.MainWindow.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.MainWindow.Name = "MainWindow";
            this.MainWindow.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("MainWindow.ObjectImage")));
            this.MainWindow.OwnedWindow = true;
            this.MainWindow.Parent = this.Zwinne2014Application;
            this.MainWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.MainWindow.UseCoordinatesOnClick = true;
            this.MainWindow.WindowClass = "";
            // 
            // TabControl1
            // 
            this.TabControl1.Comment = null;
            this.TabControl1.Index = 0;
            this.TabControl1.InstanceName = "tabControl1";
            this.TabControl1.MatchedIndex = 0;
            this.TabControl1.MsaaName = null;
            this.TabControl1.MsaaRole = System.Windows.Forms.AccessibleRole.PageTabList;
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TabControl1.ObjectImage")));
            this.TabControl1.Parent = this.MainWindow;
            this.TabControl1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.TabControl;
            this.TabControl1.UseCoordinatesOnClick = true;
            this.TabControl1.WindowClass = "";
            // 
            // Sprawy
            // 
            this.Sprawy.Comment = null;
            this.Sprawy.Index = 0;
            this.Sprawy.InstanceName = "Sprawy";
            this.Sprawy.MatchedIndex = 0;
            this.Sprawy.MsaaName = "Sprawy";
            this.Sprawy.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.Sprawy.Name = "Sprawy";
            this.Sprawy.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Sprawy.ObjectImage")));
            this.Sprawy.Parent = this.TabControl1;
            this.Sprawy.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Sprawy.UseCoordinatesOnClick = true;
            this.Sprawy.WindowClass = "";
            this.Sprawy.WindowText = "Sprawy";
            // 
            // AddCaseButton
            // 
            this.AddCaseButton.Comment = null;
            this.AddCaseButton.Index = 4;
            this.AddCaseButton.InstanceName = "addCaseButton";
            this.AddCaseButton.MatchedIndex = 0;
            this.AddCaseButton.MsaaName = "Dodaj Sprawę";
            this.AddCaseButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.AddCaseButton.Name = "AddCaseButton";
            this.AddCaseButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddCaseButton.ObjectImage")));
            this.AddCaseButton.Parent = this.Sprawy;
            this.AddCaseButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.AddCaseButton.UseCoordinatesOnClick = false;
            this.AddCaseButton.WindowClass = "";
            this.AddCaseButton.WindowText = "Dodaj Sprawę";
            // 
            // CaseListBox
            // 
            this.CaseListBox.Comment = null;
            this.CaseListBox.Index = 3;
            this.CaseListBox.InstanceName = "caseListBox";
            this.CaseListBox.MatchedIndex = 0;
            this.CaseListBox.MsaaName = null;
            this.CaseListBox.MsaaRole = System.Windows.Forms.AccessibleRole.List;
            this.CaseListBox.Name = "CaseListBox";
            this.CaseListBox.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("CaseListBox.ObjectImage")));
            this.CaseListBox.Parent = this.Sprawy;
            this.CaseListBox.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Listbox;
            this.CaseListBox.UseCoordinatesOnClick = true;
            this.CaseListBox.WindowClass = "";
            // 
            // uiMsaa1
            // 
            this.uiMsaa1.Comment = null;
            this.uiMsaa1.MsaaName = "Klient: , sprawa: , opiekun: andrzej";
            this.uiMsaa1.Name = "uiMsaa1";
            this.uiMsaa1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("uiMsaa1.ObjectImage")));
            this.uiMsaa1.Parent = this.CaseListBox;
            this.uiMsaa1.Role = System.Windows.Forms.AccessibleRole.ListItem;
            this.uiMsaa1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Unknown;
            this.uiMsaa1.UseCoordinatesOnClick = true;
            // 
            // AddEventButton
            // 
            this.AddEventButton.Comment = null;
            this.AddEventButton.Index = 2;
            this.AddEventButton.InstanceName = "AddEventButton";
            this.AddEventButton.MatchedIndex = 0;
            this.AddEventButton.MsaaName = "Dodaj termin";
            this.AddEventButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.AddEventButton.Name = "AddEventButton";
            this.AddEventButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddEventButton.ObjectImage")));
            this.AddEventButton.Parent = this.Sprawy;
            this.AddEventButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.AddEventButton.UseCoordinatesOnClick = false;
            this.AddEventButton.WindowClass = "";
            this.AddEventButton.WindowText = "Dodaj termin";
            // 
            // AddCaseWindow
            // 
            this.AddCaseWindow.Comment = null;
            this.AddCaseWindow.InstanceName = "addCaseWindow";
            this.AddCaseWindow.MatchedIndex = 0;
            this.AddCaseWindow.MsaaName = "Dodaj Sprawę";
            this.AddCaseWindow.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.AddCaseWindow.Name = "AddCaseWindow";
            this.AddCaseWindow.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddCaseWindow.ObjectImage")));
            this.AddCaseWindow.OwnedWindow = true;
            this.AddCaseWindow.Parent = this.Zwinne2014Application;
            this.AddCaseWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.AddCaseWindow.UseCoordinatesOnClick = true;
            this.AddCaseWindow.WindowClass = "";
            // 
            // AssignedLawyer
            // 
            this.AssignedLawyer.Comment = null;
            this.AssignedLawyer.Index = 3;
            this.AssignedLawyer.InstanceName = "assignedLawyer";
            this.AssignedLawyer.MatchedIndex = 0;
            this.AssignedLawyer.MsaaName = null;
            this.AssignedLawyer.MsaaRole = System.Windows.Forms.AccessibleRole.ComboBox;
            this.AssignedLawyer.Name = "AssignedLawyer";
            this.AssignedLawyer.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AssignedLawyer.ObjectImage")));
            this.AssignedLawyer.Parent = this.AddCaseWindow;
            this.AssignedLawyer.UIObjectType = TestAutomationFX.UI.UIObjectTypes.ComboBox;
            this.AssignedLawyer.UseCoordinatesOnClick = true;
            this.AssignedLawyer.WindowClass = "";
            // 
            // AddButton
            // 
            this.AddButton.Comment = null;
            this.AddButton.Index = 0;
            this.AddButton.InstanceName = "addButton";
            this.AddButton.MatchedIndex = 0;
            this.AddButton.MsaaName = "Dodaj";
            this.AddButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.AddButton.Name = "AddButton";
            this.AddButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddButton.ObjectImage")));
            this.AddButton.Parent = this.AddCaseWindow;
            this.AddButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.AddButton.UseCoordinatesOnClick = false;
            this.AddButton.WindowClass = "";
            this.AddButton.WindowText = "Dodaj";
            // 
            // Window
            // 
            this.Window.Comment = null;
            this.Window.Index = 0;
            this.Window.MatchedIndex = 0;
            this.Window.MsaaName = null;
            this.Window.MsaaRole = System.Windows.Forms.AccessibleRole.Dialog;
            this.Window.Name = "Window";
            this.Window.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Window.ObjectImage")));
            this.Window.OwnedWindow = true;
            this.Window.Parent = this.AddCaseWindow;
            this.Window.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Window.UseCoordinatesOnClick = true;
            this.Window.WindowClass = "#32770";
            // 
            // ButtonOK
            // 
            this.ButtonOK.Comment = null;
            this.ButtonOK.Index = 0;
            this.ButtonOK.MatchedIndex = 0;
            this.ButtonOK.MsaaName = "OK";
            this.ButtonOK.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("ButtonOK.ObjectImage")));
            this.ButtonOK.Parent = this.Window;
            this.ButtonOK.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.ButtonOK.UseCoordinatesOnClick = false;
            this.ButtonOK.WindowClass = "Button";
            this.ButtonOK.WindowText = "OK";
            // 
            // Window1
            // 
            this.Window1.Comment = null;
            this.Window1.Index = 18;
            this.Window1.MatchedIndex = 0;
            this.Window1.MsaaName = null;
            this.Window1.MsaaRole = System.Windows.Forms.AccessibleRole.List;
            this.Window1.Name = "Window1";
            this.Window1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Window1.ObjectImage")));
            this.Window1.OwnedWindow = true;
            this.Window1.Parent = this.Zwinne2014Application;
            this.Window1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Window1.UseCoordinatesOnClick = true;
            this.Window1.WindowClass = "ComboLBox";
            // 
            // AddEventWindow
            // 
            this.AddEventWindow.Comment = null;
            this.AddEventWindow.InstanceName = "addEventWindow";
            this.AddEventWindow.MatchedIndex = 0;
            this.AddEventWindow.MsaaName = "Dodawanie terminu";
            this.AddEventWindow.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.AddEventWindow.Name = "AddEventWindow";
            this.AddEventWindow.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddEventWindow.ObjectImage")));
            this.AddEventWindow.OwnedWindow = true;
            this.AddEventWindow.Parent = this.Zwinne2014Application;
            this.AddEventWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.AddEventWindow.UseCoordinatesOnClick = true;
            this.AddEventWindow.WindowClass = "";
            // 
            // TitleBar
            // 
            this.TitleBar.Comment = null;
            this.TitleBar.Index = 1;
            this.TitleBar.MsaaName = null;
            this.TitleBar.Name = "TitleBar";
            this.TitleBar.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TitleBar.ObjectImage")));
            this.TitleBar.Parent = this.AddEventWindow;
            this.TitleBar.Role = System.Windows.Forms.AccessibleRole.TitleBar;
            this.TitleBar.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Unknown;
            this.TitleBar.UseCoordinatesOnClick = true;
            // 
            // EventDate
            // 
            this.EventDate.Comment = null;
            this.EventDate.Index = 6;
            this.EventDate.InstanceName = "eventDate";
            this.EventDate.MatchedIndex = 0;
            this.EventDate.MsaaName = "13:50, 06 lut 2015";
            this.EventDate.MsaaRole = System.Windows.Forms.AccessibleRole.DropList;
            this.EventDate.Name = "EventDate";
            this.EventDate.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("EventDate.ObjectImage")));
            this.EventDate.Parent = this.AddEventWindow;
            this.EventDate.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.EventDate.UseCoordinatesOnClick = true;
            this.EventDate.WindowClass = "";
            this.EventDate.WindowText = "13:50, 06 lut 2015";
            // 
            // AddEventButton1
            // 
            this.AddEventButton1.Comment = null;
            this.AddEventButton1.Index = 3;
            this.AddEventButton1.InstanceName = "addEventButton";
            this.AddEventButton1.MatchedIndex = 0;
            this.AddEventButton1.MsaaName = "Dodaj";
            this.AddEventButton1.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.AddEventButton1.Name = "AddEventButton1";
            this.AddEventButton1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("AddEventButton1.ObjectImage")));
            this.AddEventButton1.Parent = this.AddEventWindow;
            this.AddEventButton1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.AddEventButton1.UseCoordinatesOnClick = false;
            this.AddEventButton1.WindowClass = "";
            this.AddEventButton1.WindowText = "Dodaj";
            // 
            // Window2
            // 
            this.Window2.Comment = null;
            this.Window2.Index = 0;
            this.Window2.MatchedIndex = 0;
            this.Window2.MsaaName = null;
            this.Window2.MsaaRole = System.Windows.Forms.AccessibleRole.Dialog;
            this.Window2.Name = "Window2";
            this.Window2.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Window2.ObjectImage")));
            this.Window2.OwnedWindow = true;
            this.Window2.Parent = this.AddEventWindow;
            this.Window2.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Window2.UseCoordinatesOnClick = true;
            this.Window2.WindowClass = "#32770";
            // 
            // BłądZładata
            // 
            this.BłądZładata.Comment = null;
            this.BłądZładata.Index = 1;
            this.BłądZładata.MatchedIndex = 0;
            this.BłądZładata.MsaaName = "Błąd: Zła data.";
            this.BłądZładata.MsaaRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.BłądZładata.Name = "BłądZładata";
            this.BłądZładata.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("BłądZładata.ObjectImage")));
            this.BłądZładata.Parent = this.Window2;
            this.BłądZładata.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.BłądZładata.UseCoordinatesOnClick = true;
            this.BłądZładata.WindowClass = "Static";
            this.BłądZładata.WindowText = "Błąd: Zła data.";
            // 
            // ButtonOK1
            // 
            this.ButtonOK1.Comment = null;
            this.ButtonOK1.Index = 0;
            this.ButtonOK1.MatchedIndex = 0;
            this.ButtonOK1.MsaaName = "OK";
            this.ButtonOK1.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.ButtonOK1.Name = "ButtonOK1";
            this.ButtonOK1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("ButtonOK1.ObjectImage")));
            this.ButtonOK1.Parent = this.Window2;
            this.ButtonOK1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.ButtonOK1.UseCoordinatesOnClick = false;
            this.ButtonOK1.WindowClass = "Button";
            this.ButtonOK1.WindowText = "OK";
            // 
            // BłądNiemanazwy
            // 
            this.BłądNiemanazwy.Comment = null;
            this.BłądNiemanazwy.Index = 1;
            this.BłądNiemanazwy.MatchedIndex = 0;
            this.BłądNiemanazwy.MsaaName = "Błąd: Nie ma nazwy.";
            this.BłądNiemanazwy.MsaaRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.BłądNiemanazwy.Name = "BłądNiemanazwy";
            this.BłądNiemanazwy.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("BłądNiemanazwy.ObjectImage")));
            this.BłądNiemanazwy.Parent = this.Window2;
            this.BłądNiemanazwy.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.BłądNiemanazwy.UseCoordinatesOnClick = true;
            this.BłądNiemanazwy.WindowClass = "Static";
            this.BłądNiemanazwy.WindowText = "Błąd: Nie ma nazwy.";
            // 
            // Window3
            // 
            this.Window3.Comment = null;
            this.Window3.Index = 0;
            this.Window3.MatchedIndex = 0;
            this.Window3.MsaaName = null;
            this.Window3.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.Window3.Name = "Window3";
            this.Window3.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("Window3.ObjectImage")));
            this.Window3.OwnedWindow = true;
            this.Window3.Parent = this.AddEventWindow;
            this.Window3.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Window3.UseCoordinatesOnClick = true;
            this.Window3.WindowClass = "DropDown";
            // 
            // CalendarControl
            // 
            this.CalendarControl.Comment = null;
            this.CalendarControl.Index = 0;
            this.CalendarControl.MatchedIndex = 0;
            this.CalendarControl.MsaaName = "Calendar Control";
            this.CalendarControl.MsaaRole = System.Windows.Forms.AccessibleRole.Client;
            this.CalendarControl.Name = "CalendarControl";
            this.CalendarControl.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("CalendarControl.ObjectImage")));
            this.CalendarControl.Parent = this.Window3;
            this.CalendarControl.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.CalendarControl.UseCoordinatesOnClick = true;
            this.CalendarControl.WindowClass = "SysMonthCal32";
            // 
            // TodayButton
            // 
            this.TodayButton.Comment = null;
            this.TodayButton.MsaaName = "Today Button";
            this.TodayButton.Name = "TodayButton";
            this.TodayButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TodayButton.ObjectImage")));
            this.TodayButton.Parent = this.CalendarControl;
            this.TodayButton.Role = System.Windows.Forms.AccessibleRole.PushButton;
            this.TodayButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.TodayButton.UseCoordinatesOnClick = false;
            // 
            // NameText
            // 
            this.NameText.Comment = null;
            this.NameText.Index = 5;
            this.NameText.InstanceName = "NameText";
            this.NameText.MatchedIndex = 0;
            this.NameText.MsaaName = null;
            this.NameText.MsaaRole = System.Windows.Forms.AccessibleRole.Text;
            this.NameText.Name = "NameText";
            this.NameText.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("NameText.ObjectImage")));
            this.NameText.Parent = this.AddEventWindow;
            this.NameText.UIObjectType = TestAutomationFX.UI.UIObjectTypes.TextBox;
            this.NameText.UseCoordinatesOnClick = true;
            this.NameText.WindowClass = "";
            // 
            // DeleteAllCasesButton
            // 
            this.DeleteAllCasesButton.Comment = null;
            this.DeleteAllCasesButton.Index = 0;
            this.DeleteAllCasesButton.InstanceName = "deleteAllCasesButton";
            this.DeleteAllCasesButton.MatchedIndex = 0;
            this.DeleteAllCasesButton.MsaaName = "Usuń Wszystkie";
            this.DeleteAllCasesButton.MsaaRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.DeleteAllCasesButton.Name = "DeleteAllCasesButton";
            this.DeleteAllCasesButton.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("DeleteAllCasesButton.ObjectImage")));
            this.DeleteAllCasesButton.Parent = this.Sprawy;
            this.DeleteAllCasesButton.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Button;
            this.DeleteAllCasesButton.UseCoordinatesOnClick = false;
            this.DeleteAllCasesButton.WindowClass = "";
            this.DeleteAllCasesButton.WindowText = "Usuń Wszystkie";
            // 
            // TitleBar1
            // 
            this.TitleBar1.Comment = null;
            this.TitleBar1.Index = 1;
            this.TitleBar1.MsaaName = null;
            this.TitleBar1.Name = "TitleBar1";
            this.TitleBar1.ObjectImage = ((System.Drawing.Bitmap)(resources.GetObject("TitleBar1.ObjectImage")));
            this.TitleBar1.Parent = this.MainWindow;
            this.TitleBar1.Role = System.Windows.Forms.AccessibleRole.TitleBar;
            this.TitleBar1.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Unknown;
            this.TitleBar1.UseCoordinatesOnClick = true;
            // 
            // Zwinne2014MainWindow
            // 
            this.Zwinne2014MainWindow.Comment = null;
            this.Zwinne2014MainWindow.Name = "Zwinne2014MainWindow";
            this.Zwinne2014MainWindow.ObjectImage = null;
            this.Zwinne2014MainWindow.Parent = this.Zwinne2014Application;
            this.Zwinne2014MainWindow.UIObjectType = TestAutomationFX.UI.UIObjectTypes.Window;
            this.Zwinne2014MainWindow.UseCoordinatesOnClick = false;
            this.Zwinne2014MainWindow.WindowClass = "";
            // 
            // terminy
            // 
            this.UIMapObjectApplications.Add(this.Zwinne2014Application);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private TestAutomationFX.UI.UIApplication Zwinne2014Application;
        private TestAutomationFX.UI.UIWindow MainWindow;
        private TestAutomationFX.UI.UIWindow TabControl1;
        private TestAutomationFX.UI.UIWindow Sprawy;
        private TestAutomationFX.UI.UIWindow AddCaseButton;
        private TestAutomationFX.UI.UIWindow CaseListBox;
        private TestAutomationFX.UI.UIMsaa uiMsaa1;
        private TestAutomationFX.UI.UIWindow AddEventButton;
        private TestAutomationFX.UI.UIWindow AddCaseWindow;
        private TestAutomationFX.UI.UIWindow AssignedLawyer;
        private TestAutomationFX.UI.UIWindow AddButton;
        private TestAutomationFX.UI.UIWindow Window;
        private TestAutomationFX.UI.UIWindow ButtonOK;
        private TestAutomationFX.UI.UIWindow Window1;
        private TestAutomationFX.UI.UIWindow AddEventWindow;
        private TestAutomationFX.UI.UIMsaa TitleBar;
        private TestAutomationFX.UI.UIWindow EventDate;
        private TestAutomationFX.UI.UIWindow AddEventButton1;
        private TestAutomationFX.UI.UIWindow Window2;
        private TestAutomationFX.UI.UIWindow BłądZładata;
        private TestAutomationFX.UI.UIWindow ButtonOK1;
        private TestAutomationFX.UI.UIWindow BłądNiemanazwy;
        private TestAutomationFX.UI.UIWindow Window3;
        private TestAutomationFX.UI.UIWindow CalendarControl;
        private TestAutomationFX.UI.UIMsaa TodayButton;
        private TestAutomationFX.UI.UIWindow NameText;
        private TestAutomationFX.UI.UIWindow DeleteAllCasesButton;
        private TestAutomationFX.UI.UIMsaa TitleBar1;
        private TestAutomationFX.UI.UIMainWindow Zwinne2014MainWindow;



    }
}
