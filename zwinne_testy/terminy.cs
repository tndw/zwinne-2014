﻿using System;
using System.ComponentModel;

using TestAutomationFX.Core;
using TestAutomationFX.UI;

namespace zwinne_testy
{
    [UITestFixture]
    public partial class terminy : UIMap
    {
        public terminy()
        {
            InitializeComponent();
        }

        public terminy(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        [UITest()]
        public void zla_data()
        {
            AddCaseButton.Click();
            AssignedLawyer.Click(86, 11);
            Window1.Click(48, 7);
            AddButton.Click();
            ButtonOK.Click();
            uiMsaa1.Click(321, 10);
            AddEventButton.Click();
            EventDate.Click(64, 19);
            Keyboard.SendKeys("[DOWN]");
            AddEventButton1.Click();
            BłądZładata.VerifyProperty("Text", "Błąd: Zła data.");
            ButtonOK1.Click();
        }

        [UITest()]
        public void dobra_data_i_nazwa()
        {
            EventDate.Click(420, 16);
            TodayButton.Click();
            AddEventButton1.Click();
            BłądNiemanazwy.VerifyProperty("Text", "Błąd: Nie ma nazwy.");
            ButtonOK1.Click();
            NameText.Click(26, 7);
            Keyboard.SendKeys("a");
            AddEventButton1.Click();
        }

        [UITest()]
        public void usun_wszystkie()
        {
            DeleteAllCasesButton.Click();
        }
    }
}
